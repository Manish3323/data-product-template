import pandas as pd
import pandasql as ps
import plotly.express as px
import delta_sharing
import numpy as np
# # Point to the profile file. It can be a file on the local file system or a file on a remote storage.
profile_file = "src/datasets.share"

# # Create a SharingClient.
client = delta_sharing.SharingClient(profile_file)

print(client.list_all_tables())

{% for item in values.entities %}

# # Create a url to access a shared table.
# # A table path is the profile file path following with `#` and the fully qualified name of a table (`<share-name>.<schema-name>.<table-name>`).
table_url${{item.table}} = profile_file + "#${{item.lob}}.${{item.dataset}}.${{item.table}}"

## comment following two lines after running it once, it will save the delta table in local pickle file, which can be used in subsequent tries.
data_${{item.table}} = delta_sharing.load_as_pandas(table_url${{item.table}})

{% endfor %}

# Analysing hockey data for 
# how many matches each team per season has played and won or lost and how many goals they scored or recieved

data_nhl_game = data_nhl_game.astype({'goalsFor': 'float', 'goalsAgainst': 'float'})

grouped_nhl_data = data_nhl_game \
  .assign(
    wins = np.where(data_nhl_game['goalsFor'] > data_nhl_game['goalsAgainst'],1,0),
    loss = np.where(data_nhl_game['goalsFor'] < data_nhl_game['goalsAgainst'],1,0),
    draws = np.where(data_nhl_game['goalsFor'] == data_nhl_game ['goalsAgainst'],1,0),
    ) \
  .groupby(['season', 'team']) \
  .agg({'wins':sum, 'loss':sum, 'draws': sum, 'goalsFor': sum, 'goalsAgainst': sum })

df2_nhl_team = grouped_nhl_data.reset_index()

# yearly analysis of each team
print(df2_nhl_team)

